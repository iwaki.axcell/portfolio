const humburger_menu = document.querySelector('#humburger-menu');
const humburger_box = document.querySelector('#humburger-box');
const face_up = document.querySelector('#js_fadeInUp');

const menu = document.querySelector('#menu');
const windowHeight = window.innerHeight;
const windowwidth = window.innerWidth;
const vCont = document.querySelector('.p-menuMovie');

// window.addEventListener('load', () => {});

humburger_menu.addEventListener('click', () => {
  humburger_menu.classList.toggle('open');
  humburger_box.classList.toggle('open');
});

window.addEventListener('scroll', () => {
  let y = window.scrollY;
  if (y >= face_up.offsetTop - windowHeight / 2) {
    face_up.classList.add('contentsOpen');
  }
});
window.addEventListener('scroll', () => {
  let y = window.scrollY;
  if (y >= menu.offsetTop - windowHeight) {
    menu.classList.add('fadeIn');
  }
});

window.addEventListener('DOMContentLoaded', () => {
  console.log(vCont.innerHTML);
  if (windowwidth >= 1000) {
    vCont.innerHTML =
      '<video src="dist/img/movie/CoffeeMachine.mp4" type="video/mp4"  playsinline loop autoplay muted id="videoContent"></video>';
    console.log(vCont.innerHTML);
  } else {
    vCont.innerHTML =
      '<video src="dist/img/movie/cupcchino.mp4" type="video/mp4"  playsinline loop autoplay muted id="videoContent"></video>';
    console.log(vCont.innerHTML);
  }
});
